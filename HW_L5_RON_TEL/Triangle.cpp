#include "Triangle.h"
#include <iostream>

//ctor for triangle
Triangle::Triangle(const Point& a, const Point& b, const Point& c, const std::string& type, const std::string& name) : Polygon(type, name)
{
	_points.push_back(a);
	_points.push_back(b);
	_points.push_back(c);
	_type = type;
	_name = name;
}

Triangle ::~Triangle () 
{
	//nothing to delete
}
void Triangle::draw(const Canvas& canvas)
{
	canvas.draw_triangle(_points[0], _points[1], _points[2]);
}

void Triangle::clearDraw(const Canvas& canvas)
{
	canvas.clear_triangle(_points[0], _points[1], _points[2]);
}

double Triangle::getArea() const
{
	return ((_points[0].getX() * (_points[1].getY() - _points[2].getY()) + _points[1].getX() * (_points[2].getY() - _points[0].getY()) + _points[2].getX() * (_points[0].getY() - _points[1].getY()))/2); //according to some formula i found online
}

double Triangle::getPerimeter() const 
{
	return (_points[0].distance(_points[1]) + _points[1].distance(_points[2]) + _points[2].distance(_points[0]));
}
