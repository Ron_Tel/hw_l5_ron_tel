#pragma once

#include "Shape.h"
#include "Point.h"
#include <vector>

class Polygon : public Shape
{
public:
	Polygon(const std::string& type, const std::string& name);

	virtual void move(const Point& other);

protected:
	std::vector<Point> _points;
};