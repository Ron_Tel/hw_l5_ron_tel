#include "Circle.h"


void Circle::draw(const Canvas& canvas)
{
	canvas.draw_circle(getCenter(), getRadius());
}

void Circle::clearDraw(const Canvas& canvas)
{
	canvas.clear_circle(getCenter(), getRadius());
}

//ctor for circle
Circle :: Circle(const Point& center, double radius, const std::string& type, const std::string& name) : Shape(name, type), _center(Point(center))
{
	_radius = radius;
	_name = name;
	_type = type;
}

Circle ::~Circle () 
{

}

//returning center
const Point& Circle :: getCenter() const 
{
	return _center;
}


double Circle :: getRadius() const 
{
	return _radius;
}

//S = PIR^2 , for some reson coudent fit to one line...
double Circle :: getArea() const 
{
	double area = _radius*PI;
	area = area*area;
	return area;
}

//2PIR
double Circle ::getPerimeter() const 
{
	return (2 * PI*_radius);
}

void Circle :: move(const Point& other) 
{
	_center += other;
}




